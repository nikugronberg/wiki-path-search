"use strict";

const cheerio = require("cheerio");
const request = require("request");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


const main = async () => {
  let start = {
    from: "",
    to: ""
  };
  let target = "";
  let baseUrl = "";

  let links = [];
  let linkPaths = [];
  let iterationLinks = [];
  let iterationArrays = [];
  let iterationDepth = 10;
  let maxParallelLinks = 100;

  // Commandline input
  rl.question("Wikipedia starting point: ", async (answer1) => {
    start.to = answer1;
    //start.to = "https://en.wikipedia.org/wiki/United_States_House_of_Representatives";
    baseUrl = linkDeconstruct(start.to);
    console.log("Starting Url: " + start.to);
    rl.question("Wikipedia ending point: ", async (answer2) => {
      target = answer2;
      //target = "https://en.wikipedia.org/wiki/Product_sample";
      console.log("Target Url: " + target);
      if(linkDeconstruct(target) != baseUrl) {
        console.log("Different types of links, shutting down.")
        return;
      }
      // Main program
      links.push(start.to);
      linkPaths.push(start);
      iterationLinks.push(start);
      iterationArrays.push(iterationLinks);
      let iterator = 0;

      // Main loop
      while(!links.includes(target) && iterator < iterationDepth) {
        console.log("Iteration depth: " + iterator);
        console.log("Looking at: " + iterationLinks.length + " links.");

        iterationLinks = [];
        let arrIterator = 0;
        for (const arr of iterationArrays) {
          console.log("New batch! Currently: " + (arrIterator+1) + "/" + iterationArrays.length);
          let someLinks = await getLinksFromList(arr, baseUrl);

          if(someLinks.map(item => item.to).includes(target)) {
            console.log("Target found! Stopping...");
            iterationLinks = iterationLinks.concat(someLinks);
            break;
          }

          iterationLinks = iterationLinks.concat(someLinks);
          arrIterator++;
        }

        console.log("New links without filtering: " + iterationLinks.length);

        // Filter out visited sites
        iterationLinks = iterationLinks.filter((item) => {
          return !links.includes(item.to);
        });

        // Remove duplicates
        iterationLinks = unique(iterationLinks);
        links = links.concat(iterationLinks.map(item => item.to));
        linkPaths = linkPaths.concat(iterationLinks);

        // Add to sliced list
        iterationArrays = [];
        for(let i = 0; i<iterationLinks.length; i+= maxParallelLinks ) {
          iterationArrays.push(iterationLinks.slice(i, i + maxParallelLinks));
        }

        console.log("All links: " + links.length + " New links: " + iterationLinks.length);
        iterator++;
      }

      if(links.includes(target)) {
        console.log("Target found! Iteration depth: " + iterator);
        let path = returnPath(linkPaths, start, target);
        console.log("Overall path:\n" + path);
      }
      else {
        console.log("Target not found! Iteration depth: " + iterator);
      }

      rl.close();
    });

  });
}

const linkDeconstruct = (link) => {
  let baseUrl = link.substr(0, 24);
  return(baseUrl);
}

const returnPath = (linkPaths, start, target) => {
  let path = target;
  let i = 0;
  while(target != start.to && i < 100) {
    let link = linkPaths.filter(item => item.to == target)[0];
    target = link.from;
    path = target + "\n --> " + path;
    i++;
  }
  return path;
}

const unique = (a) => {
  let seen = [];
  return a.filter((item) => {
    return seen.hasOwnProperty(item.to) ? false : (seen[item.to] = true);
  });
}

const getWikiPageLinks = (address, baseUrl) => {
  let cleanedLinks = [];
  return new Promise((resolve, reject) => {
    request({
      uri: address,
    }, (err, res, body) => {
      if(err) {
        console.log(err);
        return;
      }
      let $ = cheerio.load(body);
      let links = $("#mw-content-text  p > a");
      $(links).each((i, link) => {
        let linkString = $(link).attr("href");
        if(typeof linkString !== "undefined") {
          if(linkString.substr(0,5) == "/wiki") {
            let linkItem = {
              from: address,
              to: baseUrl + linkString
            };
            cleanedLinks.push(linkItem)
            //console.log(baseUrl + linkString);
          }
        }
      });
      console.log("Looked up: " + address);
      resolve(cleanedLinks);
    });
  });
}

const getLinksFromList = async (iterationArrays, baseUrl) => {
  const promises = iterationArrays.map((link) => {
    return new Promise((resolve, reject) => {
      getWikiPageLinks(link.to, baseUrl).then((newLinks) => {
        //console.log(newLinks);
        resolve(newLinks);
      });
    });
  });

  // Wait for all workers
  let promisedLinks = await Promise.all(promises);
  let iterationLinks = [];
  promisedLinks.forEach((arr) => {
    iterationLinks = iterationLinks.concat(arr);
  });
  return iterationLinks;
}


main()
